import {
    Text, View, TouchableOpacity,
    TextInput, Image, ActivityIndicator
} from 'react-native'
import React, {useState, useEffect} from 'react';
        
const Masuk = ({navigation}) =>{
        return (
            <View style={{ flex: 1, backgroundColor: '#ebef1f4',
            alignItems: 'center' }}>
                <Image source={require('./Screen.png')}
                    style={{ 
                        width: 180, height: 180,
                    }} />

                <Text style={{
                    color: '#000',
                    fontSize: 15, textAlign: 'center',
                    letterSpacing: 1
                }}>Aplikasi React Native Oleh :</Text>
                <Text style={{
                    color: '#000', fontWeight: 'bold',
                    letterSpacing: 2,
                    fontSize: 20, textAlign: 'center',
                }}>MOH. KHALIS</Text>

                <View style={{ alignItems: 'center',
                    marginTop: 150,
                    backgroundColor: '#ffffff',
                    width: 350, height: 200,
                    elevation: 2, borderRadius: 10
                }}>

                    <Text style={{
                        color: '#000',
                        fontSize: 20, textAlign: 'center',
                        marginTop: 10,
                    }}>Sebelum Masuk Tingalkan</Text>
                    <Text style={{
                        color: '#000',
                        fontSize: 20, textAlign: 'center',
                    }}>Nama Anda di Kolom Bawah</Text>

                    <TextInput style={{
                        borderWidth: 1, borderRadius: 10,
                        marginTop: 20, width: 300,}}
                        placeholder='  Nama Anda'/>

                    <TouchableOpacity
                        style={{
                            backgroundColor: '#0088f4',
                            width: 100, height: 40, marginTop: 20,
                            borderRadius: 10}}
                        onPress={() => navigation.navigate('Home')}>
                        <Text style={{
                            fontSize: 20, fontWeight: 'bold',
                            letterSpacing: 2, color: '#fff',
                            textAlign: 'center', marginTop: 9}}>
                            MASUK
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }



export default Masuk;