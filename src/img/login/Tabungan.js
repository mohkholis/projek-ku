import {
  Text, View, TouchableOpacity,
  TextInput, Alert, Image
} from 'react-native'
import React, { Component } from 'react'

class Login extends Component {
  constructor(props) {
      super(props)

      this.state = {
          username: '',
          password: '',
          isilogin: false
      }
  }

  login = () => {
      if (!this.state.username && !this.state.password)
          Alert.alert("Error", "Username dan Password harus diisi")
      else (
          this.setState({
              isilogin: true
          })
      )
  }

  render() {
      const { username, password, isilogin } = this.state;
      return (
          <View style={{ flex: 1, backgroundColor: '#ebef1f4',
          alignItems: 'center', }}>
              <Image source={require('./Screen.png')}
                  style={{
                      width: 180, height: 180,
                      marginTop: 30}} />

              <Text style={{
                  color: '#000',
                  fontSize: 15, textAlign: 'center',
                  letterSpacing: 1
              }}>Login ke</Text>
              <Text style={{
                  color: '#000', fontWeight: 'bold',
                  letterSpacing: 2,
                  fontSize: 20, textAlign: 'center',
              }}>Tabungan</Text>

              <View style={{ alignItems: 'center',
                  marginTop: 70,
                  backgroundColor: '#ffffff',
                  width: 350, height: 320,
                  elevation: 2, borderRadius: 10}}>
                  <Text style={{
                      color: '#000',
                      fontSize: 20, textAlign: 'center',
                      marginTop: 20,
                  }}>Masukkan Username & Password</Text>

                  <TextInput style={{
                      borderWidth: 1, borderRadius: 10,
                      marginTop: 30, width: 300,
                  }}
                      placeholder='  Username'
                      value={username}
                      onChangeText={(username) => this.setState({ username })}
                  />

                  <TextInput style={{
                      borderWidth: 1, borderRadius: 10,
                      marginTop: 30, width: 300,
                  }}
                      placeholder='  Pasword'
                      value={password}
                      onChangeText={(password) => this.setState({ password })}
                      secureTextEntry={true}
                  />
                  <TouchableOpacity
                      style={{
                          backgroundColor: '#0088f4',
                          width: 120, height: 40, marginTop: 30,
                          borderRadius: 10
                      }}
                      onPress={() => this.login()}>
                      <Text style={{
                          fontSize: 20, fontWeight: 'bold',
                          letterSpacing: 2, color: '#fff',
                          textAlign: 'center', marginTop: 7
                      }}>
                          Login
                      </Text>
                  </TouchableOpacity>
                  {isilogin && (
                      <Text style={{
                          marginTop: 20, color: 'red', fontSize: 15,
                      }}>
                          Username dan Password salah
                      </Text>
                  )}
              </View>
          </View>
      )
  }
}


export default Login;