import React, { useState, useEffect } from "react";
import {
  View, TouchableOpacity, StatusBar, Text,
  TextInput, ScrollView, securTextEnty
} from 'react-native';

const DompetKu = ({ navigation }) => {

  const [email, setEmail] = useState('');
  const [password, setPasword] = useState('');

  return (
    <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
      <StatusBar backgroundColor={'#0b5c98'} barStyle="light-content" />
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <View style={{
        width: 400, height: 100, backgroundColor: '#118eea'
        , elevation: 2, borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20, marginLeft: 3,
        alignItems: 'center', justifyContent: 'center'
      }}>
        <Text style={{
          fontSize: 60, fontWeight: 'bold', color: '#fff',
          textAlign: 'center', paddingTop: 10
        }}
        >SIGN IN</Text>
      </View>
      </View>


      <Text style={{
        color: '#000000', textAlign: 'center',
        letterSpacing: 1, fontSize: 20, paddingTop: 20
      }}
      >silahkan masuk jika sudah mempunyai akun
      </Text>
      <View>
        <TextInput value={email} keyboardType='email-address'
          onChangeText={text => setEmail(text)}
          style={{
            borderWidth: 1,
            marginHorizontal: 20, backgroundColor: '#ffffff', marginTop: 40,
            borderRadius: 13, elevation: 2,
            paddingLeft: 20, letterSpacing: 1,
          }}
          placeholder="Masukkan Email/No HP" />

        <TextInput value={password}
          onChangeText={text => setPasword(text)}
          style={{
            borderWidth: 1,
            marginHorizontal: 20, backgroundColor: '#ffffff', marginTop: 50,
            borderRadius: 13, elevation: 2,
            paddingLeft: 20, letterSpacing: 1,
          }}
          placeholder="Password"
          secureTextEntry={true}
        />

        <TouchableOpacity
        onPress={() => navigation.navigate('Error')}
        style={{ marginTop: 40, marginRight: 20 }}
        ><Text style={{
          fontSize: 15, color: '#000', textAlign: 'right', fontWeight: 'bold', letterSpacing: 1
        }}>
            Lupa Password?
          </Text></TouchableOpacity>

        <TouchableOpacity
        onPress={() => navigation.navigate('Error')}
        style={{
          marginTop: 30, backgroundColor: '#118eea', elevation: 2,
          paddingVertical: 20, marginHorizontal: 40, borderRadius: 20,
          justifyContent: 'center', alignItems: 'center',
          paddingTop: 10, paddingBottom: 10
        }}>
          <Text style={{
            color: '#ffffff', fontSize: 20, letterSpacing: 1,
            fontWeight: 'bold', textAlign: 'center'
          }}>Masuk</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => navigation.navigate('BuatAkun')}
          style={{
            marginTop: 40, backgroundColor: '#118eea', elevation: 2,
            paddingVertical: 20, marginHorizontal: 40, borderRadius: 20,
            justifyContent: 'center', alignItems: 'center',
            paddingTop: 10, paddingBottom: 10
          }}>
          <Text style={{
            color: '#ffffff', fontSize: 20, letterSpacing: 1,
            fontWeight: 'bold'
          }}>Daftar</Text>
        </TouchableOpacity>

      </View>
    </ScrollView>
  )
};
export default DompetKu;