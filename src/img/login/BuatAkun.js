import React, { useState, useEffect } from "react";
import {
  View, TouchableOpacity, StatusBar, Text,
  TextInput, ScrollView, securTextEnty,
} from 'react-native';
const BuatAkun = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPasword] = useState('');
  return (<ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
    <StatusBar backgroundColor={'#0b5c98'} barStyle="light-content" />
    
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
    <View style={{ 
      width: 400, height: 100, backgroundColor: '#118eea'
      , elevation: 5, borderBottomRightRadius: 20,
      borderBottomLeftRadius: 20,
    }}>
      <Text style={{
        fontSize: 60, fontWeight: 'bold', color: '#fff',
        textAlign: 'center', paddingTop: 10
      }}
      >SIGN UP</Text>
    </View>
    </View>

    <Text style={{
      color: '#000000', textAlign: 'center',
      letterSpacing: 1, fontSize: 20, paddingTop: 20
    }}
    >Selesaikan pendaftaran dibawah
    </Text>
    <View>
      <TextInput
        style={{
          borderWidth: 1,
          marginHorizontal: 20, backgroundColor: '#ffffff', marginTop: 40,
          borderRadius: 13, elevation: 4,
          paddingLeft: 20, letterSpacing: 1,
        }}
        placeholder="Nama Depan" />
      <TextInput
        style={{
          borderWidth: 1,
          marginHorizontal: 20, backgroundColor: '#ffffff', marginTop: 40,
          borderRadius: 13, elevation: 4,
          paddingLeft: 20, letterSpacing: 1,
        }}
        placeholder="dd/mm/yyyy" />

      <TextInput value={email} keyboardType='email-address'
        onChangeText={text => setEmail(text)}
        style={{
          borderWidth: 1,
          marginHorizontal: 20, backgroundColor: '#ffffff', marginTop: 40,
          borderRadius: 13, elevation: 4,
          paddingLeft: 20, letterSpacing: 1,
        }}
        placeholder="Masukkan Email/No HP" />


      <TextInput value={password}
        onChangeText={text => setPasword(text)}
        style={{
          borderWidth: 1,
          marginHorizontal: 20, backgroundColor: '#ffffff', marginTop: 50,
          borderRadius: 13, elevation: 4,
          paddingLeft: 20, letterSpacing: 1,
        }}
        placeholder="Password"
        secureTextEntry={true}
      />

      <TouchableOpacity
      onPress={() => navigation.navigate('Error')}
      style={{
        marginTop: 40, backgroundColor: '#118eea', elevation: 2,
        paddingVertical: 20, marginHorizontal: 40, borderRadius: 20,
        justifyContent: 'center', alignItems: 'center',
        paddingTop: 10, paddingBottom: 10
      }}>
        <Text style={{
          color: '#ffffff', fontSize: 20, letterSpacing: 1,
          fontWeight: 'bold', textAlign: 'center'
        }}>Masuk</Text>
      </TouchableOpacity>

      <Text style={{
        textAlign: 'center', fontSize: 15, color: '#000', fontWeight: 'bold',
        marginTop: 40, letterSpacing: 1
      }}
      >Atau login dengan</Text>

      <TouchableOpacity
      onPress={() => navigation.navigate('Error')}
      style={{
        marginTop: 20, marginLeft: 20,
        fontWeight: 'bold', fontSize: 20, letterSpacing: 1,
      }}>
        <Text style={{
          marginTop: 10, fontSize: 15, color: '#000', letterSpacing: 1,
          fontWeight: 'bold'
        }}>Facebook</Text>
      </TouchableOpacity>

      <TouchableOpacity
      onPress={() => navigation.navigate('Error')}
      style={{
        marginTop: 20, marginLeft: 20,
        fontWeight: 'bold', fontSize: 20, letterSpacing: 1,
      }}>
        <Text style={{
          marginTop: 10, fontSize: 15, color: '#000', letterSpacing: 1,
          fontWeight: 'bold'
        }}>Google</Text>
      </TouchableOpacity>

      <TouchableOpacity
      onPress={() => navigation.navigate('Error')}
      style={{
        marginTop: 20, marginLeft: 20,
        fontWeight: 'bold', fontSize: 15, color: '#000', letterSpacing: 1,
      }}>
        <Text style={{
          marginTop: 10, fontSize: 15, color: '#000', letterSpacing: 1,
          fontWeight: 'bold'
        }}>TikTok</Text>
      </TouchableOpacity>
    </View>
  </ScrollView>
  )
};
export default BuatAkun;