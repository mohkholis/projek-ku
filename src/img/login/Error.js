import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'

function Kartu({ navigation }) {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <View style={{
        backgroundColor: '#fff',
        width: 300, height: 200, elevation: 1,
      }}>
        <Text style={{
          color: 'red', fontSize: 25,
          marginLeft: 20, marginTop: 10, fontWeight: 'bold'
        }}>Error ;</Text>
        <Text
          style={{
            color: '#000', fontSize: 20, textAlign: 'center',
            marginTop: 20
          }}
        >Karena anda kurang tampan wkwkwk</Text>

        <Text
          style={{
            color: '#000', fontSize: 20, textAlign: 'center',
            marginTop: 20, fontWeight: 'bold'
          }}
        >(APLIKASI BELUM DILANJUTKAN)</Text>
        <View style={{ alignItems: 'center',}}>
        <TouchableOpacity
          style={{ alignItems: 'center',
            backgroundColor: '#fff',
            width: 250, height: 40, marginTop: 20,
            elevation: 1, borderWidth: 1, borderColor: '#e7e7e7'
          }}
          onPress={() => navigation.navigate('Masuk')}>
          <Text style={{
            fontSize: 15,
            letterSpacing: 2, color: '#000',
            textAlign: 'center', marginTop: 10
          }}>
            Kembali
          </Text>
        </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

export default Kartu;