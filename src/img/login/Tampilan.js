import React from "react";
import {StyleSheet, View, Text, Image } from 'react-native';
import { NativeScreenContainer } from "react-native-screens";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import DompetKu from "./src/img/login/DompetKu";
import SaldoKu from "./src/img/login/SaldoKu";
import ScreenSplash from "./src/img/login/ScreenSplash";
import TabunganKu from "./src/img/login/TabunganKu";
import BuatAkun from "./src/img/login/BuatAkun";
import CatatanRilis from "./src/img/login/CatatanRilis";
import html from './src/assets/images1.png';

import { NavigationContainer } from "@react-navigation/native";


const Stack = createNativeStackNavigator();

function Tampilan () {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName='ScreenSplash'>
                <Stack.Screen name='TabunganKu' component={TabunganKu}/>
                <Stack.Screen name='SaldoKu' component={SaldoKu}/>
                <Stack.Screen name='DompetKu' component={DompetKu}/>
                <Stack.Screen name='ScreenSplash' component={ScreenSplash}/>
                <Stack.Screen name='BuatAkun' component={BuatAkun}/>
                <Stack.Screen name='CatatanRilis' component={CatatanRilis}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default Tampilan;

const style = StyleSheet.create({})