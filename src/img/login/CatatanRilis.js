import { Text, View, StatusBar} from 'react-native'
import React, { Component } from 'react'

export default class CatatanRilis extends Component {
  render() {
    return (
        <View style={{ width: 580, 
            height: 100, backgroundColor: '#fff',
                borderRadius: 10, elevation: 2, marginLeft: 10, marginTop: 20}}>
            <Text style={{
                color: '#000000',  fontSize: 16,
                paddingLeft: 10,
                paddingTop: 10,
                }}>Rilis Pada Tanggal 20 Juli 2022</Text>
                <Text style={{
                color: '#000000',  fontSize: 16,
                paddingLeft: 10,
                paddingTop: 5,
                }}>Nomor Rilis (909874-682349)</Text>
                <Text style={{
                color: '#000000',  fontSize: 16,
                paddingLeft: 10,
                paddingTop: 5,
                }}>Diperbaharui :</Text>
                 <StatusBar backgroundColor={'#0b5c98'} barStyle="light-content" />
                </View>
    )
  }
}