import React from "react";
import {Image, View, Text, TouchableOpacity, StatusBar, ScrollView} from "react-native";

 const TabunganKu = ({ navigation }) => {

    return (
        <ScrollView style={{flex: 1}}>
        <View style={{ flex: 1, alignItems: 'center'}}>
            <StatusBar backgroundColor={'#0b5c98'} barStyle="light-content" />
            <View
                style={{flex: 0.2, flexDirection: 'row', borderBottomColor: '#fff',
                    width: 400, backgroundColor: '#118eea'
                    , elevation: 2, borderBottomRightRadius: 10, 
                    borderBottomLeftRadius: 10, height: 50,
                    alignItems: 'center', justifyContent: 'center',
                }}>
                <TouchableOpacity
                    onPress={() => navigation.navigate('SaldoKu')}
                    style={{ alignItems: 'center', justifyContent: 'center',
                    }}>
                    <Text style={{
                        color: '#fff', fontSize: 18, letterSpacing: 1,
                    }}>Saldo   </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('DompetKu')}
                    style={{alignItems: 'center', justifyContent: 'center',
                    }}>
                    <Text style={{
                        color: '#fff', fontSize: 18, letterSpacing: 1,
                    }}>Dompet   </Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={()=> navigation.navigate('Tabungan')}
                    style={{alignItems: 'center', justifyContent: 'center',
                    }}>
                    <Text style={{
                        color: '#fff', fontSize: 18, letterSpacing: 1,
                    }}>Tabungan   </Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={() => navigation.navigate('Error')}
                    style={{alignItems: 'center', justifyContent: 'center',
                    }}>
                    <Text style={{
                        color: '#fff', fontSize: 18, letterSpacing: 1,
                    }}>Transfer   </Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={()=> navigation.navigate('CatatanRilis') }
                    style={{alignItems: 'center', justifyContent: 'center',
                    }}>
                    <Text style={{
                        color: '#fff', fontSize: 18, letterSpacing: 1,
                    }}>Info-Rilis</Text>
                </TouchableOpacity>
            </View>

            
            <Image style={{ width: 380, 
            height: 380,
                borderRadius: 20, marginTop: 10}}
                source={require('./tabunganku.png')}/>
            <Text style={{
                color: '#000000',  fontSize: 50,
                fontWeight: 'normal', textAlign: 'center', marginTop: 30,
                fontWeight: 'bold', 
            }}
            >TabunganKu</Text>
            <Text style={{
                color: '#000000',  fontSize: 15,
                textAlign: 'center', marginTop: 10,
                letterSpacing: 1
                }}>Aplikasi React Native Oleh: MOH KHALIS</Text>
                

                <TouchableOpacity
                onPress={() => navigation.navigate('InfoAplikasi')}
                style={{width: 550, height: 40,
            marginTop: 150, elevation: 1,
                paddingTop: 10, backgroundColor: '#fff',
                borderRadius: 5,}}>
                <Text style={{ textAlign: 'center',
                    color: '#ff0000', fontSize: 15, letterSpacing: 1,
                }}>INFO APLIKASI</Text>
            </TouchableOpacity>

            <TouchableOpacity
            onPress={() => navigation.navigate('Pengaturan')}
            style={{width: 550, height: 40,
            marginTop: 2, elevation: 1,
                paddingTop: 10, backgroundColor: '#fff',
                borderRadius: 5}}>
                <Text style={{ textAlign: 'center',
                    color: '#ff0000', fontSize: 15, letterSpacing: 1,
                }}>PENGATURAN</Text>
            </TouchableOpacity>

            <TouchableOpacity
            onPress={() => navigation.navigate('Berinilai')}
            style={{width: 550, height: 40,
            marginTop: 2, elevation: 1,
                paddingTop: 10, backgroundColor: '#fff',
                borderRadius: 5,}}>
                <Text style={{ textAlign: 'center',
                    color: '#ff0000', fontSize: 15, letterSpacing: 1,
                }}>BERI NILAI</Text>
            </TouchableOpacity>

            <TouchableOpacity
            onPress={() => navigation.navigate('Error')}
            style={{width: 100, height: 40,
            marginTop: 150, elevation: 2,
                paddingTop: 10, backgroundColor: '#118eea',
                borderRadius: 10,
            }}>
                <Text style={{ textAlign: 'center',
                    color: '#ffffff', fontSize: 15, letterSpacing: 1,
                    fontWeight: 'bold'
                }}>contact us</Text>
            </TouchableOpacity>

            <Text style={{
                textAlign: 'center', fontSize: 15, fontWeight: 'bold',
                marginTop: 10, letterSpacing: 1
            }}
            >Atau hubungi No: 087865253950</Text>
        </View>
        </ScrollView>
    );
    
}

export default TabunganKu;
