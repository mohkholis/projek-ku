import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, StatusBar, TouchableOpacity } from 'react-native';

const SaldoKu = ({ navigation }) => {
  return (
    <View style={{ flex: 1, alignItems: 'center' }}>
      <StatusBar barStyle='light-content' backgroundColor='#0b5c98' />
      <View style={{
        width: 380, height: 270, backgroundColor: '#118eea',
        marginTop: 10, borderRadius: 5,
        elevation: 1
      }}>
        <Text style={{
          fontSize: 20, fontWeight: 'bold',
          color: '#fff', paddingTop: 15, paddingLeft: 10,
          letterSpacing: 1
        }}>SALDO</Text>
        <Text style={{
          fontSize: 55, fontWeight: 'bold', color: '#fff',
          textAlign: 'left', paddingTop: 20, paddingLeft: 10,
        }}>
          Rp -
        </Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Error')}
          style={{
            borderWidth: 3, borderRadius: 5, width: 100,
            paddingTop: 10,
            marginTop: 55,marginLeft: 250, borderColor: '#fff'
          }}>
          <Text style={{ color: '#fff', fontSize: 15, marginBottom: 7, marginLeft: 10 }}>
            + TOP UP
          </Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
      onPress={() => navigation.navigate('Error') }
      style={{
        borderWidth: 3, borderRadius: 5, width: 200,
        paddingTop: 10, marginTop: 15, elevation: 2,
         borderColor: '#118eea'
      }}>
        <Text style={{
          textAlign: 'center', color: '#118eea', fontSize: 15,
          marginBottom: 7
        }}>
          Tambahkan Kartu Baru
        </Text>
      </TouchableOpacity>
    </View>
  );
}


export default SaldoKu;