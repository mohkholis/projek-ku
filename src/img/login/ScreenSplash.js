import React, {Component} from "react";
import { View, Text, Image, StatusBar } from "react-native";
import {StackActions} from '@react-navigation/native';

class TabunganKu extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentDidMount(){
        setTimeout(()=> {
            this.props.navigation.dispatch(StackActions.replace('Masuk'))
        }, 3000);
    }


render() {
    return(
    <View style={{flex: 1, justifyContent: 'center',
    alignItems: 'center'}}>
        <Image style={{
        width: 250, height: 250,}}
        source={require('./Screen.png')}
        />
        <Text style={{textAlign: 'center',fontSize: 15, paddingTop: 10}}>
            Aplikasi react-native, yang dibuat langsung oleh : </Text>
        <Text style={{textAlign: 'center',fontSize: 20, fontWeight: 'bold',
        letterSpacing: 2}}>
            MOH. KHALIS</Text>
    </View>
    );
}
}

export default TabunganKu;