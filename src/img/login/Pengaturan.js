import { Text, View, TouchableOpacity } from 'react-native'
import React from 'react'

function Pengaturan({navigation}) {
    return (
      <View
      style={{flex: 1}}>
        <TouchableOpacity
        onPress={() => navigation.navigate('Error') }
        style={{backgroundColor: '#ffffff', 
      width: 700, height: 80, elevation: 2,}}>
        <Text style={{color: '#000', 
      letterSpacing: 1, marginTop: 15, fontSize: 20,
      fontWeight: 'bold', marginLeft: 10}}>
          Umum
        </Text>
        <Text style={{fontSize: 15, marginLeft: 10,
        letterSpacing: 1, color: '#000'}}>
          Prefensi akun, notifikasi
        </Text>
        </TouchableOpacity>

        <TouchableOpacity
        onPress={() => navigation.navigate('Error') }
        style={{backgroundColor: '#ffffff', 
      width: 700, height: 80, elevation: 2,
      marginTop: 2}}>
        <Text style={{color: '#000', 
      letterSpacing: 1, marginTop: 15, fontSize: 20,
      fontWeight: 'bold', marginLeft: 10}}>
          Autentikasi
        </Text>
        <Text style={{fontSize: 15, marginLeft: 10,
        letterSpacing: 1, color: '#000'}}>
          Sidik jari, autentikasi pembelian
        </Text>
        </TouchableOpacity>

        <TouchableOpacity
        onPress={() => navigation.navigate('Error') }
        style={{backgroundColor: '#ffffff', 
      width: 700, height: 80, elevation: 2,
      marginTop: 2}}>
        <Text style={{color: '#000', 
      letterSpacing: 1, marginTop: 15, fontSize: 20,
      fontWeight: 'bold', marginLeft: 10}}>
          Keluarga
        </Text>
        <Text style={{fontSize: 15, marginLeft: 10,
        letterSpacing: 1, color: '#000'}}>
          Kontrol orang tua, panduan orang tua
        </Text>
        </TouchableOpacity>

        <TouchableOpacity
        onPress={() => navigation.navigate('Error') }
        style={{backgroundColor: '#ffffff', 
      width: 700, height: 80, elevation: 2,
      marginTop: 2}}>
        <Text style={{color: '#000', 
      letterSpacing: 1, marginTop: 15, fontSize: 20,
      fontWeight: 'bold', marginLeft: 10}}>
          Tentang
        </Text>
        <Text style={{fontSize: 15, marginLeft: 10,
        letterSpacing: 1, color: '#000'}}>
          Versi buld, sertifikasi perangkat
        </Text>
        </TouchableOpacity>
      </View>
    )
  }
 export default Pengaturan;