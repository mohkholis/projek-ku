import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'

export default function BeriNilai({navigation}) {
    return (
        <View>
            <Text style={{color: '#000', fontSize: 25, textAlign: 'center',
            letterSpacing: 1, marginTop: 300, fontWeight: 'bold'}}>Beri Nilai</Text>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <TouchableOpacity 
                onPress={() => navigation.navigate('Terimakasih')}
                style={{backgroundColor: 'yellow',
            width: 60, height: 60, borderRadius: 30, margin: 5,
            borderColor: '#000', borderEndWidth: 1, elevation: 1}}>
                    <Text style={{color: '#000', fontSize: 25,
            textAlign: 'center', marginTop: 15, fontWeight: 'bold'}}>1</Text>
                </TouchableOpacity>
                
                <TouchableOpacity 
                onPress={() => navigation.navigate('Terimakasih')}
                style={{backgroundColor: 'yellow',
            width: 60, height: 60, borderRadius: 30, margin: 5,
            borderColor: '#000', borderEndWidth: 1, elevation: 1}}>
                    <Text style={{color: '#000', fontSize: 25,
            textAlign: 'center', marginTop: 15, fontWeight: 'bold'}}>2</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                onPress={() => navigation.navigate('Terimakasih')}
                style={{backgroundColor: 'yellow',
            width: 60, height: 60, borderRadius: 30, margin: 5,
            borderColor: '#000', borderEndWidth: 1, elevation: 1}}>
                    <Text style={{color: '#000', fontSize: 25,
            textAlign: 'center', marginTop: 15, fontWeight: 'bold'}}>3</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                onPress={() => navigation.navigate('Terimakasih')}
                style={{backgroundColor: 'yellow',
            width: 60, height: 60, borderRadius: 30, margin: 5,
            borderColor: '#000', borderEndWidth: 1, elevation: 1}}>
                    <Text style={{color: '#000', fontSize: 25,
            textAlign: 'center', marginTop: 15, fontWeight: 'bold'}}>4</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                onPress={() => navigation.navigate('Terimakasih')}
                style={{backgroundColor: 'yellow',
            width: 60, height: 60, borderRadius: 30, margin: 5,
            borderColor: '#000', borderEndWidth: 1, elevation: 1}}>
                    <Text style={{color: '#000', fontSize: 25,
            textAlign: 'center', marginTop: 15, fontWeight: 'bold'}}>5</Text>
                </TouchableOpacity>
            </View>
        </View>
        </View>
    )
}