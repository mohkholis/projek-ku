import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";

import DompetKu from "./src/img/login/DompetKu";
import SaldoKu from "./src/img/login/SaldoKu";
import ScreenSplash from "./src/img/login/ScreenSplash";
import Home from "./src/img/login/Home";
import BuatAkun from "./src/img/login/BuatAkun";
import CatatanRilis from "./src/img/login/CatatanRilis";
import Tabungan from "./src/img/login/Tabungan";
import Pengaturan from "./src/img/login/Pengaturan";
import Masuk from "./src/img/login/Masuk";
import Error from "./src/img/login/Error";
import BeriNilai from "./src/img/login/BeriNilai";
import Terimakasih from "./src/img/login/Terimakasih";
import InfoAplikasi from "./src/img/login/InfoAplikasi";


const Khalis = createNativeStackNavigator();

function App () {
    return (
        <NavigationContainer>
            <Khalis.Navigator initialRouteName='ScreenSplash'>
                <Khalis.Screen name='Error' component={Error}/>
                <Khalis.Screen name='InfoAplikasi' component={InfoAplikasi}/>
                <Khalis.Screen name='Terimakasih' component={Terimakasih}/>
                <Khalis.Screen name='Berinilai' component={BeriNilai}/>
                <Khalis.Screen name='Masuk' component={Masuk}/>
                <Khalis.Screen name='Tabungan' component={Tabungan}/>
                <Khalis.Screen name='Pengaturan' component={Pengaturan}/>
                <Khalis.Screen name='SaldoKu' component={SaldoKu}/>
                <Khalis.Screen name='Home' component={Home}/>
                <Khalis.Screen name='DompetKu' component={DompetKu}/>
                <Khalis.Screen name='ScreenSplash' component={ScreenSplash}/>
                <Khalis.Screen name='BuatAkun' component={BuatAkun}/>
                <Khalis.Screen name='CatatanRilis' component={CatatanRilis}/>
            </Khalis.Navigator>
        </NavigationContainer>
    );
}

export default App;